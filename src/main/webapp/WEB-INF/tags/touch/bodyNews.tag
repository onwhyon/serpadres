<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag description="Texto largo de la noticia" %>

<%@ attribute name="text" description="Texto completo de la descripción del contenido" %>
<div class="container">
	<p class="news-long-text-mod">${text}</p>
</div>