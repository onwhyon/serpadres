﻿<%@ tag language="java" pageEncoding="UTF-8"
description="OJO!!!!!!! Si se usa este tag hay que cerrar el div: &lt;div id='content' class='page-content'&gt; al final de la página ya que si no los elementos no se encajan correctamente" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="menuTitle" description="Título del menú" %>
<%@ attribute name="list" type="java.util.Iterator" description="Lista de las secciones (normálmente el listado de los contenidos de la categoría padre del portal)" %>
<%@ attribute name="image" description="Imágen del menu" %>
<%@ attribute name="imageAlt" description="Alt para las imágenes a mostrar" %>
<%@ attribute name="pag" description="'Página' destino para las url friendly" %>

<div id="sidebar" class="page-sidebar">
	<div class="page-sidebar-scroll">
		<div class="sidebar-header">
			<a href="#" class="delete-sidebar"><span></span></a>
		</div>

		<ul>
			<li><a href="#" class="nav-item logo active"><em class="nav-onit"></em></a></li>
			<li><a href="${pageContext.request.contextPath}/index.htm" class="nav-item_home type-nav">Inicio<em class="nav-page"></em></a></li>
			<c:forEach items="${list }" var="item" varStatus="status">
				<c:set var="itemlink" value="" />
				<c:set var="itemtitle" value="" />
				<c:forEach items="${item.texts }" var="aux">
					<c:choose>
						<c:when test="${aux.idTextType == 2 }"><c:set var="itemtitle" value="${aux.text }" /></c:when>
						<c:when test="${fn:length(itemlink) <= 0 && aux.idTextType == 15 }"><c:set var="itemlink" value="${aux.text }" /></c:when>
						<c:when test="${aux.idTextType == 20 }"><c:set var="itemlink" value="${pag }/${aux.text }" /></c:when>
					</c:choose>
				</c:forEach>
				<c:forEach items="${item.images }" var="aux">
					<c:if test="${aux.idImageType == 6 }"><c:set var="itemimage" value="${aux.path }" /></c:if>
				</c:forEach>
				<c:set var="icoClass" value="${(fn:length(itemimage)>0) ? itemimage : 'type-nav' }" />
				<c:choose>
					<c:when test="${status.count < 6}">
						<li>
							<a href="${pageContext.request.contextPath}/${itemlink }" class="nav-item ${icoClass } ${(status.last)?'no-border':''}">     ${itemtitle }<em class="nav-page"></em></a>
						</li>
					</c:when>
					<c:otherwise>
						<c:if test="${status.count == 6 }">
							<li>
							<a class="nav-item navdesktop">Mas<em class="nav-page imgDesplegable"></em></a>
							<ul>

						</c:if>
						<li>
							<a href="${pageContext.request.contextPath}/${itemlink }" class="nav-item ${icoClass } ${(status.last)?'no-border':''} navSublink">     ${itemtitle }<em class="nav-page"></em></a>
						</li>
						<c:if test="${status.last }">
							</ul>
							</li>
						</c:if>
					</c:otherwise>
				</c:choose>
		</c:forEach>
		</ul>
	</div>
</div>

<div id="content" class="page-content">
	<div class="page-header">
		<img class="logo">
		<a href="/index.htm" class="deploy-sidebar" id="botonS"><span></span></a>
	</div>