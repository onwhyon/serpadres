<%@ tag language="java" pageEncoding="UTF-8" description="Usa los mismos datos que article3colonlytext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../views/inc/ctes.jsp"%>

<%@ attribute name="title" %>
<%@ attribute name="list" type="java.util.Iterator" %>

<c:forEach items="${list }" var="item" varStatus="status">
	<c:if test="${status.first }">	
		<c:if test="${fn:length(title)>0 }">
			<div class="column no-bottom">
			    <div class="one-half-responsive">
			       <h4 class="uppercase">${title }</h4>
			    </div>
			</div>
		</c:if>
			<div class="one-half-responsive">
				<div class="code">
	</c:if>
	<c:set var="itemtitle" value="noencontrado tipo 2" />
	<c:set var="itemlink" value="" />

	<c:forEach items="${item.texts }" var="aux">
		<c:choose>
			<c:when test="${aux.idTextType == 2 }"><c:set var="itemtitle" value="${aux.text }" /></c:when>
			<c:when test="${fn:length(itemlink) <= 0 && aux.idTextType == 15 }"><c:set var="itemlink" value="${aux.text }" /></c:when>
			<c:when test="${aux.idTextType == 20 }"><c:set var="itemlink" value="${aux.text }" /></c:when>
		</c:choose>
	</c:forEach>

	<c:set var="class" value="pinkline" />
	<c:if test="${status.count % 2 == 0}"><c:set var="class" value="darkpink" /></c:if>
	<a href="//${itemlink }"><span class="${class }">${itemtitle }</span></a>

	<c:if test="${status.last }">
			</div>
	  </div>        
	</c:if>
</c:forEach>
