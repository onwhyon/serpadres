<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="title" description="" %>
<%@ attribute name="text" description="" %>
<%@ attribute name="image" description="" %>
<%@ attribute name="imgalt" description="" %>
<%@ attribute name="link" description="" %>
<%@ attribute name="friendurl" description="" %>

<c:set var="itemlink" value="${(fn:length(friendurl) <= 0)?link:friendurl }" />

<c:if test="${fn:length(itemlink) > 0 }">
	<a href="${pageContext.request.contextPath}/${itemlink }">
</c:if>
<div class="container">
	<h2 class="center-text"><span class="highlight" style="color:rgba(255,0,0,1);">${title }</h2>
	<p  class="no-bottom" style="padding-bottom:15px;" >${text }</p> 
</div>
<div style="margin-bottom:25px;">
	<img src="${pageContext.request.contextPath}/resources/${image }" alt="${imgalt}" style="width:100%;">
</div>
<c:if test="${fn:length(itemlink) > 0 }">
	</a>
</c:if>