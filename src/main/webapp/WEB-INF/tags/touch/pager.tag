<%@ tag language="java" pageEncoding="UTF-8"%>

<div class="pager-mod">
	<div class="column">
		<div class="one-half pag-prev-mod">
			<a href="" class="flat-button">Prev</a>
		</div>
		<div class="one-half pag-next-mod">
			<a href="" class="flat-button">Next</a>
		</div>
	</div>
</div>
