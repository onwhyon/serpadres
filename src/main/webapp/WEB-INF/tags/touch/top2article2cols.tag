<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="owo" tagdir="/WEB-INF/tags/touch" %>

<%@ attribute name="list" description="Lista con los dos artículos destacados" type="java.util.Iterator" %>
<%@ attribute name="maxelems" description="Número de elementos a mostrar, por defecto 2" type="java.lang.Integer" %>
<%@ attribute name="pag" description="'Página' destino para las url friendly" %>

<c:set var="maxelems" value="${(maxelems > 0 ? maxelems : 2)-1 }" />
<div class="column">
	<c:forEach items="${list }" var="item" varStatus="status"  end="${ maxelems}" >
		<c:set var="class" value="${!((status.count % 2 == 1) && status.last) ? 'one-half' : 'container' }" />
		<c:set var="itemimage" value="" />
		<c:set var="itemtitle" value="" />
		<c:set var="itemimgalt" value="" />
		<c:set var="itemtext" value="" />
		<c:forEach items="${item.texts }" var="aux">
			<c:choose>
				<c:when test="${aux.idTextType == 2 }"><c:set var="itemtitle" value="${aux.text }" /></c:when>
				<c:when test="${aux.idTextType == 4 }"><c:set var="itemtext" value="${aux.text }" /></c:when>
				<c:when test="${aux.idTextType == 16 }"><c:set var="itemimgalt" value="${aux.text }" /></c:when>
				<c:when test="${fn:length(itemlink) <= 0 && aux.idTextType == 15 }"><c:set var="itemlink" value="${aux.text }" /></c:when>
				<c:when test="${aux.idTextType == 20 }"><c:set var="itemlink" value="${pag}/${aux.text }" /></c:when>
			</c:choose>
		</c:forEach>
		<c:forEach items="${item.images }" var="aux">
			<c:if test="${aux.idImageType == 6 }"><c:set var="itemimage" value="${aux.path }" /></c:if>
		</c:forEach>
		<div class="${class }">
			<a href="${pageContext.request.contextPath}/${itemlink }">
				<img class="center-icon" src="${pageContext.request.contextPath}/resources/${itemimage }" alt="${itemimgalt }">
				<h3 class="center-text uppercase">${itemtitle }</h3>
				<p class="center-text no-bottom">
					${itemtext }
				</p> 
			</a>
		</div>
		<c:if test="${(status.count % 2 == 0)&&!status.last&&!status.first}">
			</div><div class="column">
		</c:if>
	</c:forEach>
</div>

