﻿<%@ tag language="java" pageEncoding="UTF-8"
	description="Usa los mismos datos que article1row"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="../../views/inc/ctes.jsp"%>

<%@ attribute name="title"%>
<%@ attribute name="list" type="java.util.Iterator"%>
<%@ attribute name="pag"
	description="'Página' destino para las url friendly"%>

<c:if test="${fn:length(title) > 0 }">
	<h4 class="heading center-text highlight bg-yellow">${title }</h4>
</c:if>
<c:forEach items="${list }" var="item">
	<c:set var="itemtitle" />
	<c:set var="itemttext" />
	<c:set var="itemlink" />
	<c:set var="itemimgalt" />
	<c:set var="itemimage" />
	<c:set var="itemcat" />
	
	<c:forEach items="${item.texts }" var="aux">
		<c:choose>
			<c:when test="${aux.idTextType == 2 }">
				<c:set var="itemtitle" value="${aux.text }" />
			</c:when>
			<c:when test="${aux.idTextType == 5 }">
				<c:set var="itemtext" value="${aux.text }" />
			</c:when>
			<c:when test="${fn:length(itemlink) <= 0 && aux.idTextType == 15 }">
				<c:set var="itemlink" value="${aux.text }" />
			</c:when>
			<c:when test="${aux.idTextType == 20}">
				<c:set var="itemlink" value="${pag }/${aux.text }" />
			</c:when>
			<c:when test="${aux.idTextType == 16 }">
				<c:set var="itemimgalt" value="${aux.text }" />
			</c:when>
			<c:when test="${aux.idTextType == 17 }">
				<c:set var="itemcat" value="${aux.text }" />
			</c:when>
		</c:choose>
	</c:forEach>

	<c:forEach items="${item.images }" var="aux">
		<c:if test="${fn:length(itemimage) <= 0 && aux.idImageType == 2 }">
			<c:set var="itemimage" value="${aux.path }" />
		</c:if>
		<c:if test="${aux.idImageType == 6 }">
			<c:set var="itemimage" value="${aux.path }" />
		</c:if>
	</c:forEach>
	
	<div class="column box">
		<h3>${itemcat }</h3>
		<c:if test="${fn:length(itemlink) > 0}">
			<a href="${pageContext.request.contextPath}/${itemlink }">
		</c:if>
		<img src="<%=IMGS_PATH %>${itemimage }">
		<h4><b>${itemtitle }</b></h4>
		<p class="no-bottom grosso">${itemtext }</p>

		<c:if test="${fn:length(itemlink) > 0}">
			</a>
		</c:if>
		<a class="ver_mas margin-top" href="${pageContext.request.contextPath}/${itemlink }">Leer [+]</a>
	</div>
</c:forEach>