﻿<%@ tag language="java" pageEncoding="UTF-8" description="Usa los mismos datos que article3colonlytext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="title" %>
<%@ attribute name="list" type="java.util.Iterator" %>
<%@ attribute name="pag" description="'Página' destino para las url friendly" %>

<div class="fooTer">
	<ul class="fooTerNavi1">
		<c:forEach items="${list }" var="item" varStatus="status">
		
			<c:set var="itemtitle" value="noencontrado tipo 2" />
			<c:set var="itemlink" value="" />
		
			<c:forEach items="${item.texts }" var="aux">
				<c:choose>
					<c:when test="${aux.idTextType == 2 }"><c:set var="itemtitle" value="${aux.text }" /></c:when>
					<c:when test="${fn:length(itemlink) <= 0 && aux.idTextType == 15 }"><c:set var="itemlink" value="${aux.text }" /></c:when>
					<c:when test="${aux.idTextType == 20 }"><c:set var="itemlink" value="${pag }/${aux.text }" /></c:when>
				</c:choose>
			</c:forEach>
			<li>
				<a href="${pageContext.request.contextPath}/${itemlink }">${itemtitle }</a>
			</li>
		</c:forEach>
	</ul>
	<ul class="fooTerNavi2">
		<li><a href="${pageContext.request.contextPath}/condicionesservicio.htm">Condiciones legales</a></li>
			<li><strong>® Dieta y Salud</strong>. All Rights Reserved</li>
	</ul>
</div>