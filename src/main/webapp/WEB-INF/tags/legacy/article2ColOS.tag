<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://example.com/functions" prefix="f" %>
<%@ include file="../../views/inc/ctes.jsp"%>

<%@ attribute name="title" description="Título del listado" %>
<%@ attribute name="list" type="java.util.Iterator" description="Iterador con los elementos a mostrar" %>
<%@ attribute name="maxelems" description="Número de elementos a mostrar, por defecto 2" type="java.lang.Integer" %>
<%@ attribute name="pag" %>
<%@ attribute name="site" %>
<c:set var="maxelems" value="${(maxelems > 0 ? maxelems : 2)-1 }" />

<c:if test="${fn:length(title)!=0 }">
	 <table style="text-align: center;"> 
		<tr>
			<td>
				<h4 class="titles">${title}</h4>
			</td>
		</tr>
	</table> 
 </c:if>

<table class="colorCategorias">
<c:forEach items="${list }" var="item" varStatus="status" end="${ maxelems}">
	<c:set var="itemtitle" value="" />
	<c:set var="itemttext" value="" />
	<c:set var="itemlink" value="" />
	<c:set var="itemimgalt" value="" />
	<c:set var="itemimage" value="" />
	
	<c:forEach items="${item.texts }" var="aux">
		<c:choose>
			<c:when test="${aux.idTextType == 17 }"><c:set var="itemtitle" value="${aux.text }" /></c:when>
			<c:when test="${aux.idTextType == 4 }"><c:set var="itemttext" value="${aux.text }" /></c:when>
			<c:when test="${aux.idTextType == 20 }"><c:set var="itemlink" value="${pag }/${aux.text }" /></c:when>
			<c:when test="${aux.idTextType == 16 }"><c:set var="itemgimgalt" value="${aux.text }" /></c:when>
			<c:when test="${aux.idTextType == 15 and (fn:length(itemlink)==0)}"><c:set var="itemlink" value="premium/${aux.text }" /></c:when>
		</c:choose>
		
	</c:forEach>
	<c:forEach items="${item.images }" var="aux">
		<c:if test="${aux.idImageType == 6 }"><c:set var="itemimage" value="${aux.path }" /></c:if>
	</c:forEach>
	<c:if test="${status.first || (status.count%2==1) }">
	<tr>
		
	</c:if>

	<c:choose> 
	     <c:when test="${status.last && (status.count % 2 == 1)}" >
	    	<td class="tableExpand" colspan="2" style="text-align:center;">
	    </c:when>
	    <c:otherwise>
	    	<td class="tableColumn" style="text-align:center;">
	    </c:otherwise>
   </c:choose>
   
    	 <a href="${itemlink}">
			<h4 class="boldTtitles">${itemtitle }</h4>
			<img src="<%=IMGS_PATH %>${itemimage }" alt="${itemimgalt }" class="tableExpand" style="margin:0 auto;">
			<p>${itemttext }</p> 
		</a>
	</td>
	<!-- Segundo de la columna -->
	<c:if test="${status.last || (status.count%2==0) }">
		</tr>
	</c:if>
	<c:set var="itemlink" value="" />
</c:forEach>
</table>