<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag description="Texto largo de la noticia" %>

<%@ attribute name="text" description="Texto completo de la descripción del contenido" %>

<table >
	<tr>
		<td>
		<p>${text}</p>
		</td>
	</tr>
</table>