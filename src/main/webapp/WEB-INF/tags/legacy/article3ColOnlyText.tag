<%@ tag language="java" pageEncoding="UTF-8"
description=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="list" type="java.util.Iterator" description="Listado de los elementos a mostrar" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<table>
	
		<c:forEach items="${list }" var="item" varStatus="status" >
			<c:set var="itemtitle" value="" />
			<c:set var="itemtext" value="" />
			<c:set var="itemlink" value="" />
			<c:forEach items="${item.texts }" var="aux">
				<c:choose>
					<c:when test="${aux.idTextType == 2}"><c:set var="itemtitle" value="${aux.text }" /></c:when>
					<c:when test="${aux.idTextType == 15}"><c:set var="itemlink" value="${aux.text }" /></c:when>
				</c:choose>
			</c:forEach>
		<c:if test="${status.first || ((status.index)%2==0) }">
		<tr>
	</c:if>	
	
	    <td  style="text-align:center;">  
	    <a href="${pageContext.request.contextPath}${itemlink }">
	 
	        <h4 class="titles">${itemtitle }</h4>
	       </a>
	    </td>
	   
	    
	    <c:if test="${status.last }">
		</tr>
	</c:if>	
		</c:forEach>
</table>