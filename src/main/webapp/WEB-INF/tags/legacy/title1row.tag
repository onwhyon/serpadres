<%@ tag language="java" pageEncoding="UTF-8"%>

<%@ attribute name="title" description="Texto que irá en h2 y centrado" %>

<table style="text-align: center;">
	<tr>
		<td class="encuesta">
		<h2 >${title }</h2>
		</td>
	</tr>
</table>