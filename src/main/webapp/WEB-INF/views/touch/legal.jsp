﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/touch" prefix="owo"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="../inc/ctes.jsp"%>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<%@ include file="../inc/headTouch.jsp"%>
<body>

	<owo:headerMenu image="${headermenu_0_106 }"
		imageAlt="${headermenu_0_016 }" list="${headermenucats }"
		menuTitle="${headermenu_0_002 }" pag="<%=CAT %>" />

	<div class="container top">
		<c:if test="${condiciones=='true'}">
			<p ALIGN="CENTER">
				<strong>Condiciones particulares del Servicio</strong>
			</p>
			<p ALIGN="CENTER">
				<BR>
			</p>
			<p ALIGN="JUSTIFY">Vodafone España, con domicilio social en
				Avenida de América, 115, 28042 Madrid, informa al cliente que para
				el disfrute de los distintos contenidos prestados a través de
				Vodafone, puede optar, en algunos casos por el pago único,
				suscripciones o bonos, siempre contra su saldo o factura Vodafone.</p>
			<p ALIGN="JUSTIFY">Para formalizar la contratación solicitada,
				sólo tiene que pulsar el botón “CONFIRMAR COMPRA” que encontrará en
				cada página de cobro, aceptación que será archivada por Vodafone.</p>
			<p ALIGN="JUSTIFY">Vodafone no responderá ni por daños directos,
				ni indirectos, ni por el daño emergente, ni por el lucro cesante,
				por los eventuales perjuicios derivados del uso de la información y
				contenidos accesibles o contenidos de terceros a través de las
				conexiones y accesos existentes</p>
			<p ALIGN="JUSTIFY">Asimismo, Vodafone no será en ningún caso
				responsable, ni siquiera de forma directa o subsidiaria de productos
				o servicios prestados u ofertados por otras personas o entidades, o
				por contenidos, informaciones, comunicaciones, opiniones o
				manifestaciones de cualquier tipo originados o vertidos por terceros
				y que resulten accesibles a través de las páginas de los portales
				que ofrece Vodafone.</p>
			<br>
			<p ALIGN="JUSTIFY">
				<a
					href="${pageContext.request.contextPath}/<%= request.getParameter("return") %>">Volver</a>
			</p>
		</c:if>
		<c:if test="${privacidad=='true'}">
			<p ALIGN="CENTER">
				<strong>Politica de privacidad</strong>
			</p>
			<p ALIGN="JUSTIFY">Vodafone España, S.A.U., entidad de
				nacionalidad española, con domicilio social en Avenida de América,
				115, 28042 Madrid, provista de C.I.F. A-809073967 e inscrita ante el
				Registro Mercantil de Madrid (“Vodafone”) le informa de que el
				contenido que usted ha seleccionado y cualquier otro incluido dentro
				de Vodafone es prestado por el tercero cuyo nombre aparece en la
				carta de pago. Este tercero es responsable de este contenido, de
				forma que Vodafone no responderá por ningún perjuicio derivado del
				uso y disfrute del mismo. Asimismo, Vodafone no será en ningún caso
				responsable, ni siquiera de forma indirecta o subsidiaria, por
				productos o servicios prestados u ofertados por otras personas o
				entidades, o por contenidos, informaciones, comunicaciones,
				opiniones o manifestaciones de cualquier tipo originados o vertidos
				por terceros y que resulten accesibles a través de los sistemas de
				Vodafone.</p>
			<p ALIGN="JUSTIFY">Vodafone le informa de que para la
				contratación de los contenidos de terceros usted puede optar por el
				pago único o a través de suscripciones, siempre utilizando los
				sistemas de cobro de Vodafone. Así, Vodafone le cargará el cobro de
				los contenidos en su saldo de prepago o en su factura de contrato.
				Para formalizar la contratación solicitada, usted sólo tiene que
				pulsar el botón COMPRAR/SUSCRIBIR que encontrará en cada página de
				cobro.</p>
			<p ALIGN="JUSTIFY">La aceptación de la contratación de cualquiera
				de los contenidos de Vodafone que será archivada por Vodafone
				implica necesariamente su consentimiento a que Vodafone ceda a
				aquellos terceros cuyo nombre aparece en la carta de pago los datos
				necesarios para la formalización de la contratación y para la
				prestación de los contenidos.</p>
		</c:if>
		<c:if test="${condiciones=='true'}">
			<p ALIGN="CENTER">
				<strong>Terminos y condiciones</strong>
			</p>
			<p ALIGN="CENTER">Servicio disponible para usuarios Vodafone a
				nivel nacional. Al suscribirse se entenderá que el usuario ha leído,
				entendido y aceptado los Términos y Condiciones del servicio “NOMBRE
				SERVICIO” y semanalmente obtendrá descargas ilimitadas que podrá
				utilizar para acceder y/o descargar contenidos del mismo. El costo
				semanal del servicio se hará con cargo a su factura (Usuarios
				Postpago) o a su Saldo de Prepago (Usuarios Prepago). Para dar de
				baja el servicio entra en miweb.vodafone.es -> secciones -> mis
				cosas -> Mis bonos y suscripciones; elige la suscripción que desees
				cancelar y después presiona Finalizar suscripción. Todos los costos
				incluyen IVA del 21%. Responsable de la prestación del servicio:
				GENERA MOBILE S.A.</p>
			<p ALIGN="CENTER">Limitación de responsabilidad por medio de esta
				advertencia, Genera Mobile S.A., sus subsidiarias y/o afiliadas,
				informan a los usuarios de las páginas de Genera Mobile S.A., (en
				adelante los “usuarios”), que a través de éstas pone a su
				disposición dispositivos técnicos de enlace (tales como, entre
				otros, links, banners, botones), directorios y herramientas de
				búsqueda que les permiten acceder a páginas web pertenecientes a
				terceros (en adelante los “sitios enlazados”). La instalación de
				estos enlaces en las páginas de Genera Mobile S.A., sus subsidiarias
				y/o afiliadas se limita a facilitar a los usuarios la búsqueda de, y
				acceso, a la información disponible de los sitios enlazados en
				internet, y no presupone que exista ninguna clase de vínculo o
				asociación entre Genera Mobile S.A., sus subsidiarias y/o afiliadas,
				y los operadores de los sitios enlazados. Genera Mobile S.A., sus
				subsidiarias y/o afiliadas no controlan, aprueban ni hacen propios
				los servicios, información, datos, archivos, productos y cualquier
				clase de material existente en los sitios enlazados. El usuario, por
				lo tanto, debe extremar la prudencia en la valoración y utilización
				de los servicios, información, datos, archivos, productos y
				cualquier clase de material existente en los sitios enlazados.
				Genera Mobile S.A., sus subsidiarias y/o afiliadas, no garantizan ni
				asumen responsabilidad alguna por los daños y perjuicios de toda
				clase que puedan causarse por:</p>
			<ul>
				<li>El funcionamiento, disponibilidad, accesibilidad o
					continuidad de los sitios enlazados.</li>
				<li>El mantenimiento de los servicios, información, datos,
					archivos, productos y cualquier clase de material existente en los
					sitios enlazados</li>
				<li>Las obligaciones y ofertas existentes en los sitios
					enlazados</li>
			</ul>

			<p ALIGN="CENTER">Utilidad mediante la utilización de esta
				página, el usuario reconoce y acepta que Genera Mobile S.A., sus
				subsidiarias y/o afiliadas, no garantizan que los sitios enlazados o
				la información proporcionada por terceros, sean útiles para la
				realización de ninguna actividad en particular. Mediante el uso de
				esta página, el usuario reconoce y acepta que Genera Mobile S.A.,
				sus subsidiarias y/o afiliadas quedan excluidas de cualquier
				responsabilidad por los daños y perjuicios que pudieran haber sido
				causados por la veracidad de la información o calidad de servicios
				contenidos u ofrecidos por terceros o que se encuentre en los sitios
				enlazados, o los que surjan con relación a este sitio, tanto el
				acceso a esta página como el uso que pueda hacerse de la información
				contenida en el mismo son exclusiva responsabilidad del usuario.</p>
			<p ALIGN="CENTER">Seguridad mediante el uso de esta página, el
				usuario reconoce y acepta que Genera Mobile S.A., sus subsidiarias
				y/o afiliadas, no garantizan la seguridad de los sitios enlazados, y
				en particular, que los usuarios puedan efectivamente acceder a las
				distintas páginas web que representan los sitios enlazados, ni que a
				través de éstos puedan transmitir, difundir, almacenar o poner a
				disposición de terceros su contenido. Mediante el uso de esta
				página, el usuario reconoce y acepta que Genera Mobile S.A., sus
				subsidiarias y/o afiliadas, quedan excluidas de cualquier
				responsabilidad por los daños y perjuicios de toda naturaleza que
				pudieran ser causados por la falta de seguridad de los sitios
				enlazados.</p>
			<p ALIGN="CENTER">Calidad mediante el uso de esta página, el
				usuario reconoce y acepta que Genera Mobile S.A., sus subsidiarias
				y/o afiliadas, no controlan y no garantizan la ausencia de virus en
				el contenido de los sitios enlazados, ni la ausencia de otros
				elementos que pudieran producir alteraciones en sus sistemas
				informáticos o en los documentos electrónicos y archivos almacenados
				en sus sistemas informáticos.</p>
			<p ALIGN="CENTER">Propiedad intelectual mediante el uso de esta
				página, el usuario reconoce y acepta que Genera Mobile S.A., sus
				subsidiarias y/o afiliadas, quedan excluidas de cualquier
				responsabilidad que pudiera ser causada por el uso no autorizado de
				las marcas u otros derechos de propiedad intelectual de terceros o
				contenidos en los sitios enlazados. De igual manera, las eventuales
				referencias que se hagan en esta página a cualquier producto,
				servicio, proceso, sitio enlazado, hipertexto o cualquier otra
				información en la que se utilicen marcas, signos distintivos y/o
				dominios, el nombre comercial o el nombre del fabricante,
				suministrador, etc., que sean titularidad de terceros, en ningún
				momento constituirán, ni implicarán respaldo o recomendación alguna
				por parte de Genera Mobile S.A., sus subsidiarias y/o afiliadas y en
				ningún caso Genera Mobile S.A., sus subsidiarias y/o afiliadas se
				asignan propiedad ni responsabilidad sobre los mismos.</p>
			<p ALIGN="CENTER">Elementos de las páginas es probable que para
				proporcionar la información contenida en el la página web tal como
				dibujos, diseños, sonido, videos, textos, fotografías, etc., Genera
				Mobile S.A., sus subsidiarias y/o afiliadas, hubieren contratado a
				terceros para realizar los estudios e investigaciones
				correspondientes, así como los dibujos, diseños, sonidos, videos,
				textos, o fotografías, que se muestren en la página. Genera Mobile
				S.A., sus subsidiarias y/o afiliadas, advierten que al no ser de su
				titularidad, ni desarrollo toda la información contenida en la
				página web, algunos de los textos, gráficos, vínculos y/o el
				contenido de algunos artículos incluidos en la misma, podrían no ser
				veraces o no estar actualizados, por lo que Genera Mobile S.A., sus
				subsidiarias y/o afiliadas, no se hacen responsables.</p>
			<p ALIGN="CENTER">Ley aplicable y jurisdicción el usuario al
				hacer uso de las páginas en internet de Genera Mobile S.A., sus
				subsidiarias y/o afiliadas, acepta de manera expresa, someterse en
				caso de cualquier controversia, a la jurisdicción de los tribunales
				de Sevilla, España, así como las leyes aplicables para el caso
				concreto vigentes en dicho lugar, renunciando expresamente a
				cualquier otra jurisdicción que por motivo de su nacionalidad o
				domicilio pudiera corresponder.</p>
			<p ALIGN="CENTER">DERECHOS DE AUTOR</p>
			<p ALIGN="CENTER">Condiciones de Uso de los Contenidos</p>
			<p ALIGN="CENTER">“El (los) Contenido(s) que usted adquiere y los
				derechos conexos correspondientes son propiedad intelectual valiosa
				de sus respectivos titulares y usted puede utilizarlos
				exclusivamente en su teléfono móvil en la que han sido ofertados, ya
				sea como un “tono” musical, protector de pantalla, fondo de pantalla
				o para reproducirlo única y exclusivamente en su teléfonos celular y
				para efectos personales. Queda estrictamente prohibido el transmitir
				dicho Contenido desde su teléfono móvil, independientemente del
				medio o finalidad de la transmisión. Asimismo, se prohíbe
				expresamente la copia, redistribución o uso diverso de los
				Contenidos a aquel para el cual han sido ofertados”.</p>
			<p ALIGN="CENTER">Las aclaraciones sobre el servicio, en especial
				las relacionadas con contenido no recibido, serán atendidas
				directamente en cualquier representante del centro de atención.</p>
			<p ALIGN="CENTER">Genera Mobile no bonificará el importe de un
				contenido solicitado desde equipos que no soporten el servicio, por
				lo que queda bajo la responsabilidad del usuario solicitar
				contenidos desde equipos que no estén dentro de los señalados en
				este portal.</p>
			<p ALIGN="CENTER">Debido a los diferentes formatos que manejan
				los teléfonos, así como a las particularidades y capacidades
				técnicas de los mismos, las melodías que se escuchan a manera de
				ejemplo a través de esta página pueden tener variaciones respecto de
				la forma en la que se escucharían en el teléfono al momento de ser
				ejecutadas en el mismo.</p>
			<p ALIGN="CENTER">*En adelante la palabra “contenido” se refiere
				a los servicios que el usuario puede descargar a su teléfono y que
				están publicados en el portal de Vodafone</p>
			<p ALIGN="CENTER">CONDICIONES GENERALES</p>
			<p ALIGN="CENTER">Estas condiciones generales (en adelante, las
				“Condiciones Generales”) regulan el uso del Portal “NOMBRE DEL
				SERVICIO” (en adelante, el “CONTENIDO”) que Genera Mobile pone
				gratuitamente a disposición de los usuarios de Internet. La
				utilización del CONTENIDO atribuye la condición de usuario de la
				misma (en adelante, el “Usuario”) y expresa la aceptación plena y
				sin reservas del Usuario de todas y cada una de las Condiciones
				Generales en la versión publicada por Genera Mobile en el momento
				mismo en que el Usuario acceda al CONTENIDO. En consecuencia, el
				Usuario debe leer atentamente las Condiciones Generales en cada una
				de las ocasiones en que se proponga utilizarla. La utilización del
				servicio se encuentra sometida a todos los avisos, reglamentos de
				uso e instrucciones puestos en conocimiento del Usuario por Genera
				Mobile, que completan lo previsto en estas Condiciones Generales en
				cuanto no se opongan a ellas.</p>
			<p ALIGN="CENTER">OBJETO</p>
			<p ALIGN="CENTER">A través del CONTENIDO, Genera Mobile facilita
				a los Usuarios el acceso y la utilización de diversos servicios y
				contenidos puestos a disposición de los Usuarios por Genera Mobile o
				por terceros usuarios del CONTENIDO y/o terceros proveedores de
				servicios y contenidos (en adelante, los “Servicios”). Genera Mobile
				se reserva el derecho a modificar unilateralmente, en cualquier
				momento y sin aviso previo, la presentación y configuración del
				CONTENIDO, así como los Servicios y las condiciones requeridas para
				utilizar y los Servicios.</p>
			<p ALIGN="CENTER">CONDICIONES DE ACCESO Y UTILIZACIÓN DEL
				CONTENIDO</p>
			<p ALIGN="CENTER">3.1. Carácter gratuito del acceso y utilización
				del CONTENIDO. La prestación del servicio del CONTENIDO por parte de
				Genera Mobile puede tener carácter gratuito para los Usuarios y no
				exige la previa suscripción o registro del Usuario. No obstante, la
				utilización de algunos Servicios sólo puede hacerse mediante
				suscripción o registro del Usuario.</p>
			<p ALIGN="CENTER">3.2. Obligación de hacer un uso correcto de la
				sección Entretenimiento y de los Servicios El Usuario se compromete
				a utilizar el CONTENIDO y los Servicios de buena fe, de conformidad
				con lo establecido en la Ley, en estas Condiciones Generales, así
				como con la moral y buenas costumbres generalmente aceptadas, en
				especial las normas de comportamiento en Internet y el orden
				público. El Usuario se obliga a abstenerse de utilizar el CONTENIDO
				y los Servicios con fines o efectos ilícitos, contrarios a lo
				establecido en estas Condiciones Generales, lesivos de los derechos
				e intereses de terceros, o que de cualquier forma puedan dañar,
				inutilizar, sobrecargar o deteriorar el CONTENIDO y los Servicios o
				impedir la normal utilización o disfrute de los mismos por parte de
				los Usuarios.</p>
			<p ALIGN="CENTER">3.3. Medios para la obtención de Contenidos El
				Usuario deberá abstenerse de obtener e incluso de intentar obtener
				informaciones, mensajes, gráficos, dibujos, archivos de sonido y/o
				imagen, fotografías, grabaciones, software y, en general, cualquier
				clase de material accesibles a través del CONTENIDO o de los
				Servicios (en adelante, los “Contenidos”) empleando para ello medios
				o procedimientos distintos de los que, según los casos, se hayan
				puesto a su disposición a este efecto o se hayan indicado a este
				efecto en las páginas web donde se encuentren los Contenidos o, en
				general, de los que se empleen habitualmente en Internet a este
				efecto siempre que no entrañen un riesgo de daño o inutilización del
				CONTENIDO, de los Servicios y/o de los Contenidos.</p>
			<p ALIGN="CENTER">3.4. Uso correcto de los Contenidos. El Usuario
				se obliga a usar los Contenidos de buena fe, de forma diligente,
				correcta y lícita y, en particular, se compromete a abstenerse de:</p>
			<ul>
				<li>utilizar los Contenidos de forma, con fines o efectos
					contrarios a la ley, a la moral y a las buenas costumbres
					generalmente aceptadas, en especial las normas de comportamiento en
					Internet o al orden público;</li>
				<li>reproducir o copiar, distribuir, permitir el acceso del
					público a través de cualquier modalidad de comunicación pública,
					transformar o modificar los Contenidos, a menos que se cuente con
					la autorización del titular de los correspondientes derechos o ello
					resulte legalmente permitido;</li>
				<li>suprimir, eludir o manipular el “copyright” y demás datos
					identificativos de los derechos de Genera Mobile o de sus titulares
					incorporados a los Contenidos;</li>
				<li>emplear los Contenidos y, en particular, la información de
					cualquier clase obtenida a través del CONTENIDO o de los Servicios
					para remitir publicidad, comunicaciones con fines de venta directa
					o con cualquier otra clase de finalidad comercial, mensajes no
					solicitados dirigidos a una pluralidad de personas con
					independencia de su finalidad, así como a abstenerse de
					comercializar o divulgar de cualquier modo dicha información;</li>
				<li>en general, infringir de cualquier forma los derechos de
					propiedad intelectual y/o industrial de Genera Mobile y de terceros
					cuyos contenidos se hallen alojados en el “NOMBRE DEL SERVICIO”.</li>
			</ul>
			<p ALIGN="CENTER">UTILIZACIÓN DEL CONTENIDO, DE LOS SERVICIOS Y
				DE LOS CONTENIDOS BAJO LA EXCLUSIVA RESPONSABILIDAD DEL USUARIO</p>
			<p ALIGN="CENTER">El Usuario es consciente de y acepta
				voluntariamente que el uso del “NOMBRE DEL SERVICIO”, de los
				Servicios y de los Contenidos tiene lugar, en todo caso, bajo su
				única y exclusiva responsabilidad.</p>
			<p ALIGN="CENTER">EXCLUSIÓN DE GARANTÍAS Y DE RESPONSABILIDAD</p>
			<ul>
				<li>EL INCUMPLIMIENTO DE LA LEY, LA MORAL Y LAS BUENAS
					COSTUMBRES GENERALMENTE ACEPTADAS, EN ESPECIAL LAS NORMAS DE
					COMPORTAMIENTO EN INTERNET O EL ORDEN PÚBLICO COMO CONSECUENCIA DE
					LA TRANSMISIÓN, DIFUSIÓN, ALMACENAMIENTO, PUESTA A DISPOSICIÓN,
					RECEPCIÓN, OBTENCIÓN O ACCESO A LOS CONTENIDOS;</li>
				<li>LA INFRACCIÓN DE LOS DERECHOS DE PROPIEDAD INTELECTUAL E
					INDUSTRIAL, DE LOS SECRETOS EMPRESARIALES, DE COMPROMISOS
					CONTRACTUALES DE CUALQUIER CLASE, DE LOS DERECHOS AL HONOR, A LA
					INTIMIDAD PERSONAL Y FAMILIAR Y A LA IMAGEN DE LAS PERSONAS, DE LOS
					DERECHOS DE PROPIEDAD Y DE TODA OTRA NATURALEZA PERTENECIENTES A UN
					TERCERO COMO CONSECUENCIA DE LA TRANSMISIÓN, DIFUSIÓN,
					ALMACENAMIENTO, PUESTA A DISPOSICIÓN, RECEPCIÓN, OBTENCIÓN O ACCESO
					A LOS CONTENIDOS;</li>
				<li>LA REALIZACIÓN DE ACTOS DE COMPETENCIA DESLEAL Y PUBLICIDAD
					ILÍCITA COMO CONSECUENCIA DE LA TRANSMISIÓN, DIFUSIÓN,
					ALMACENAMIENTO, PUESTA A DISPOSICIÓN, RECEPCIÓN, OBTENCIÓN O ACCESO
					A LOS CONTENIDOS;</li>
				<li>LA FALTA DE VERACIDAD, EXACTITUD, EXHAUSTIVIDAD,
					PERTINENCIA Y/O ACTUALIDAD DE LOS CONTENIDOS;</li>
				<li>LA INADECUACIÓN PARA CUALQUIER CLASE DE PROPÓSITO DE Y LA
					DEFRAUDACIÓN DE LAS EXPECTATIVAS GENERADAS POR LOS CONTENIDOS;</li>
				<li>EL INCUMPLIMIENTO, RETRASO EN EL CUMPLIMIENTO, CUMPLIMIENTO
					DEFECTUOSO O TERMINACIÓN POR CUALQUIER CAUSA DE LAS OBLIGACIONES
					CONTRAÍDAS POR TERCEROS Y CONTRATOS REALIZADOS CON TERCEROS A
					TRAVÉS DE O CON MOTIVO DEL ACCESO A LOS CONTENIDOS;</li>
				<li>LOS VICIOS Y DEFECTOS DE TODA CLASE DE LOS CONTENIDOS
					TRANSMITIDOS, DIFUNDIDOS, ALMACENADOS, PUESTOS A DISPOSICIÓN O DE
					OTRA FORMA TRANSMITIDOS O PUESTOS A DISPOSICIÓN, RECIBIDOS,
					OBTENIDOS O A LOS QUE SE HAYA ACCEDIDO A TRAVÉS DEL CONTENIDO O DE
					LOS SERVICIOS.</li>
				<li></li>
			</ul>
			<p ALIGN="CENTER">5.2. Exclusión de garantías y de
				responsabilidad por la utilización del CONTENIDO, de los Servicios y
				de los Contenidos por los Usuarios Genera Mobile no tiene obligación
				de y no controla la utilización que los Usuarios hacen del
				CONTENIDO, de los Servicios y de los Contenidos. En particular,
				Genera Mobile no garantiza que los Usuarios utilicen el CONTENIDO,
				los Servicios y los Contenidos de conformidad con estas Condiciones
				Generales, ni que lo hagan de forma diligente y prudente. Genera
				Mobile tampoco tiene la obligación de verificar y no verifica la
				identidad de los Usuarios, ni la veracidad, vigencia, exhaustividad
				y/o autenticidad de los datos que los Usuarios proporcionan sobre sí
				mismos a otros Usuarios. Genera Mobile NO SE RESPONSABILIZA POR LOS
				DAÑOS Y PERJUICIOS DE TODA NATURALEZA QUE PUDIERAN DEBERSE A LA
				UTILIZACIÓN DE LOS CONTENIDOS Y DE LOS CONTENIDOS POR PARTE DE LOS
				USUARIOS.</p>
			<p ALIGN="CENTER">NO LICENCIA</p>
			<p ALIGN="CENTER">Genera Mobile no concede ninguna licencia o
				autorización de uso de ninguna clase sobre sus derechos de propiedad
				industrial e intelectual o sobre cualquier otra propiedad o derecho
				relacionado con el CONTENIDO, los Servicios o los Contenidos.</p>
			<p ALIGN="CENTER">DURACIÓN Y TERMINACIÓN</p>
			<p ALIGN="CENTER">La prestación del servicio del CONTENIDO y de
				los demás Servicios tiene, en principio, una duración indefinida.
				VODAFONE, no obstante, está autorizado para dar por terminada o
				suspender la prestación del servicio del CONTENIDO y/o de cualquiera
				de los Servicios en cualquier momento. Cuando ello sea
				razonablemente posible, VODAFONE advertirá previamente la
				terminación o suspensión de la prestación del servicio del
				CONTENIDO.</p>
			<p ALIGN="CENTER">TÉRMINOS Y CONDICIONES</p>
			<p ALIGN="CENTER">Estos Términos y Condiciones de Uso entrarán en
				vigor a partir del momento en que sean publicados en el Sitio o
				notificadas al Usuario. Comprando el Contenido o suscribiéndose al
				Servicio, usted acepta cumplir con este Acuerdo, y reconoce ser
				titular de una línea contratada con VODAFONE o tener la aprobación
				del titular de la misma. Si usted no está de acuerdo con los
				Términos de este Acuerdo, no proceda a ingresar o utilizar los
				Servicios o Contenidos. Genera Mobile puede cambiar los términos de
				este Acuerdo en cualquier momento, notificándolo dentro del Sitio.
				Contenido se refiere a toda la información, imágenes, tonos, o
				vídeos disponibles en este Sitio. Sitio se refiere al CONTENIDO, el
				cual está sujeto a este Acuerdo.</p>
			<p ALIGN="CENTER">Una vez que se haya completado el proceso de
				registro y aceptado este Acuerdo, usted puede acceder a los
				Servicios (definidos abajo) y consultar o descargar Contenido a su
				dispositivo móvil por medio de conexión a internet (Red VODAFONE,
				Wi-Fi) desde el Sitio sujeto a los términos de este Acuerdo. Una vez
				que el Usuario haya comprado el Contenido, éste podrá ser descargado
				o visualizado tantas veces como el usuario quiera mientras la
				suscripción se encuentre activa. Los Servicios y Contenidos están
				dirigidos a usuarios VODAFONE.</p>
			<p ALIGN="CENTER">Los siguientes servicios están disponibles para
				su uso:</p>
			<p ALIGN="CENTER">Servicio con costo semanal/mensual de 1,21€ IVA
				incluido, con el cual obtendrá accesos y descargas ilimitadas
				semanalmente que le permitirá adquirir Contenido del catálogo
				disponible dentro del Servicio El Servicio se activará únicamente en
				el momento en el que el Usuario lo solicite.</p>
			<p ALIGN="CENTER">Se puede registrar dando clic al botón
				“SUSCRIBIR” (o su equivalente) en la página de detalle de Contenido
				disponible y/o en micrositios de las promociones vigentes. Genera
				Mobile puede modificar o quitar el contenido ofrecido como parte de
				los Servicios en cualquier momento sin notificar.</p>
			<p ALIGN="CENTER">Costos y Pago:</p>
			<p ALIGN="CENTER">Costo semanal del servicio 1,21€ IVA incluido,
				si accedes por primera vez a la oferta, la primera semana es gratis
				Cualquier costo se hará con cargo a su factura (Usuarios Pospago) o
				a su Saldo Prepago (Usuarios Prepago) y al precio indicado en el
				Sitio. Una vez cobrado, se permitirá el uso del Servicio. No habrá
				reembolsos por uso parcial del Servicio.</p>
			<p ALIGN="CENTER">Duración y Cancelación</p>
			<p ALIGN="CENTER">El Servicio permanecerá
				activo, salvo indicación en contrario del Usuario, el cual podrá
				darlo de baja en cualquier momento entrando en: miweb.vodafone.es ->
				secciones -> mis cosas -> Mis bonos y suscripciones; eligiendo la
				suscripción que desea cancelar y después presionando Finalizar
				suscripción Una vez dado de baja, perderá todas las descargas que no
				se hubieran usado antes de cancelar el Servicio. Genera Mobile se
				reserva el derecho a cancelar su suscripción en cualquier momento.</p>
		</c:if>
	</div>
	</div>
</body>
</html>
