<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/touch" prefix="owo"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../inc/ctes.jsp"%>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<%@ include file="../inc/headTouch.jsp"%>
<body>

	<owo:headerMenu image="${headermenu_0_106 }"
		imageAlt="${headermenu_0_016 }" list="${headermenucats }"
		menuTitle="${headermenu_0_002 }" pag="<%=CAT %>" />

	<div class="moduloPrincipal" style="margin-top: 31px">

		<div>
			<img alt="${banner_1_016 }" src="<%=IMGS_PATH %>${banner_1_101 }" />
		</div>

		<h4>${title1row_1_002 }</h4>

		<p class="no-bottom grosso">${intronews_1_005 } </p>

		<owo:taskList list="${tasklist_1_008 }" title="${tasklist_1_007 }" />

		<p class="txtArticulo">${bodynews_1_006 }</p>

	</div>

	<div class="artNavi">
		<c:if test="${prev==true }">
			<a href="${prvcnt}"><span>Anterior</span></a>
		</c:if>
		<c:if test="${prev==false }">
			<a href="${prvcnt}" style="display: none"><span>Anterior</span></a>
		</c:if>
		<c:if test="${sig==true }">
			<a href="${nxtcnt}"><span>Siguiente</span></a>
		</c:if>
		<c:if test="${sig==false }">
			<a href="${nxtcnt}" style="display: none;"><span>Siguiente</span></a>
		</c:if>
	</div>
	<owo:catList1col list="${article2col_0 }" pag="<%=CAT %>"/>
</div>
</body>
</html>