﻿<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags/legacy" prefix="owo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file = "../inc/ctes.jsp" %>

<!DOCTYPE html PUBLIC "-/W3C/DTD XHTML 1.0 Transitional/EN" "http:/www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<%@ include file="../inc/headPml.jsp" %>
	<body>
		<owo:headerMenu image="${headermenu_0_106 }" imageAlt="${headermenu_0_016 }" />
		<table>
			<tr>
				<td>
					<p>
						Somos padres<br><br>
						Suscripción: 7 días renovable<br>
						Precio: 1,21 (IVA incl.)<br>
					</p>
				</td>
			</tr>
		</table>

		<table class="encuesta-butt-mod">
			<tr>
				<td class="tableColumn">
				<a href="${subscribe }" class="botonGreen"> Confirmar compra </a>
			</tr>
		</table>

		<table>
			<tr>
				<td>
					<p>
						Si clicas en Comprar aceptas las <a href="${pageContext.request.contextPath}/condicionesservicio.htm?return=${return }">Condiciones legales</a>
							<br><br>
					<a href="${pageContext.request.contextPath}/index.htm">Cancelar</a>	
					</p>
				</td>
			</tr>
		</table>
	</body>
</html>