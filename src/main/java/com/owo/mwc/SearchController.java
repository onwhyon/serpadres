package com.owo.mwc;

import com.owo.dao.domain.category.Category;
import com.owo.dao.domain.content.Content;
import com.owo.dao.repository.category.CategoryDao;
import com.owo.dao.repository.content.ContentDao;
import com.owo.mwc.parsed.Article2Col;
import com.owo.mwc.parsed.Article2ColOS;
import com.owo.mwc.parsed.HeaderMenu;
import com.owo.web.http.Params;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Controller
public class SearchController extends PadresController {

	@Autowired
	HeaderMenu headermenu;
	@Autowired
	Article2ColOS article2ColOS;
	@Autowired
	Article2Col article2col;

	@Autowired
	CategoryDao catdao;
	@Autowired
	ContentDao cntdao;

	@RequestMapping(value="/search.htm", method=RequestMethod.POST)
	public String procVote(Model model, HttpServletRequest request,
			@RequestParam(value = "txt") String searchtxt) {

		String sRet = "redirect:index.htm";
		if(StringUtils.isEmpty(searchtxt))
			searchtxt = request.getParameter("txt");
		if(StringUtils.isNotEmpty(searchtxt)) {
			long alternativesearch = 0;
			sRet=checkCommons(request);
			try{
				sesdao.InsertSearch(searchtxt, ows.getSite().getIdSite());
			}catch(Exception e){
				log.log(Level.ERROR, "Error saving search, exception: " + e.getMessage());
			}
			Category cat = null;
			boolean hayResultados = false;

			if (param != null && !param.noParams()) {
				if (gama!=null) {
					List<Content> cnts = new ArrayList<Content>();
					try {
						headermenu.getParsed(model, param);
						
						final Properties properties = new Properties();
						properties.load(this.getClass().getResourceAsStream("/META-INF/search.properties"));
						cnts = cntdao.getSearch(searchtxt, 1, (String)properties.get("cat.search"));
						cat=catdao.getCategory(catMenu);
						if(cnts.size() == 0) {
							cnts = cntdao.getCntsHome(1,0,10,true,0);
						} else
							hayResultados = true;
						if(cnts.size() > 0)
							sRet = gama + "search";
					} catch (IOException ioe) {
					} catch (Exception e) {
						cnts = cntdao.getCntsHome(1,0,10,true,0);
						sRet = gama + "search";
					}
					if (StringUtils.isNotEmpty(searchtxt))
						model.addAttribute("searchstring", searchtxt);
	
					if (cat != null && cat.getIdCategory() > 0 && cnts != null && cnts.size() > 0) {
						article2ColOS.setContent(cat, cnts);
						article2ColOS.getParsed(model, param);
					} else 
						 sRet = "redirect:index.htm";
			
					if (hayResultados)
						model.addAttribute(Params.TITLE1ROWCA + "_002", "Resultados para: " + searchtxt);
					else {
						model.addAttribute(Params.TITLE1ROWCA + "_002", "0 resultados con: " + searchtxt);
						model.addAttribute("searcherrtxt", "No se han encontrado resultados con tu busqueda, prueba a buscar por palabras sueltas ");
					}
				}
			}
		}

		article2col.paramCat();
		article2col.setCategory(cMenu, listMenu);
		article2col.getParsed(model, param);

		model.addAttribute("subscribed", subscrito);
		return sRet;
	}

	@RequestMapping(value="/search.htm", method=RequestMethod.GET)
	public String nada() {
		String sRet = "redirect:index.htm";
		return sRet;
	}
}
