package com.owo.mwc;

import com.owo.dao.domain.Constants;
import com.owo.dao.domain.category.Category;
import com.owo.dao.domain.content.Content;
import com.owo.mwc.parsed.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
public class IndexController extends PadresController {

	@Autowired
	HeaderMenu headermenu;
	@Autowired
	MainSlider mainslider;
	@Autowired
	TopText toptext;
	@Autowired
	Article2ColOS article2colos;
	@Autowired
	Banner banner;
	@Autowired
	Article2Col article2col;
	@Autowired
	PollParser poll;
	@Autowired
	Title title;

	@RequestMapping(value = {"/","/index.htm*"}, method = RequestMethod.GET)
	public String index(Model model, HttpServletRequest request) {

		String sRet = checkCommons(request);
		if (redirected){
			log.log(Level.INFO, "STOPPING CONTROLLER, RETURNING NULL");
			return "";
		}
		if (StringUtils.isNotEmpty(sRet) && sRet.contains("/svsession/"))
			return sRet;

		headermenu.setCategory(cMenu, listMenu);
		headermenu.getParsed(model, param);

		model.addAttribute("subscribed", subscrito);
		model.addAttribute("wifi", wifi);

		Content dest = cntdao.getCntDestHome(true, ows.getSite().getIdSite());
		long idCntDest = 0;
		List <Content> cntsHome = null;
		if (dest!=null){
			idCntDest=dest.getIdContent();
			cntsHome = cntdao.getCntsHome(catMenu, 0, limitepags, true, idCntDest);
		}else{
			cntsHome = cntdao.getCntsHome(catMenu, 0, limitepags+1, true, idCntDest);
			dest=cntsHome.get(0);
			cntsHome= cntsHome.subList(1, cntsHome.size());
		}

		banner.paramCnt();
		banner.setContent(dest);
		banner.getParsed(model, param, "_1");

		Set<Category> cat = null;
		try{
			cat = dest.getCategories();
			if (cat!=null){
				Category destCat = cat.iterator().next();
				long idCatDest=destCat.getIdCategory();
				model.addAttribute("catDest", textdao.getCatTextByType(idCatDest,(long)Constants.TEXT_ID_TITLE).getText());
			}
		}catch(Exception e){
			log.log(Level.ERROR, "Error en categoria: " + e.getMessage());
		}
		
		article2colos.paramCnt();
		article2colos.setContent(cMenu, cntsHome);
		article2colos.getParsed(model, param, "_1");

		article2col.paramCat();
		article2col.setCategory(cMenu, listMenu);
		article2col.getParsed(model, param);

		Category catOS = catdao.getCategory(catDestacda);

		toptext.paramCat();
		toptext.setCategory(catOS);
		toptext.getParsed(model, param);

		Content cntVid=null;
		List <Content> cntsVid = null;
		cntVid=cntdao.getCntVidHome(true,ows.getSite().getIdSite());

		if (cntVid!=null){
			cntsVid=new ArrayList <Content>();
			cntsVid.add(cntVid);

			article2colos.paramCnt();
			article2colos.setContent(cMenu, cntsVid);
			article2colos.getParsed(model, param, "_3");
		}

		return gama + "index";
	}
}
