package com.owo.mwc;

import com.owo.dao.domain.Constants;
import com.owo.dao.domain.category.Category;
import com.owo.dao.domain.content.Content;
import com.owo.mwc.parsed.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
public class CategoryController extends PadresController {

	@Autowired
	HeaderMenu headermenu;
	@Autowired
	MainSlider mainslider;
	@Autowired
	Article2ColOS article2colos;
	@Autowired
	Article2Col article2col;
	@Autowired
	Title title;
	@Autowired
	Banner banner;
	
	@RequestMapping(value = {"/category.htm*", "/category/*"}, method = RequestMethod.GET)
	public String category(Model model, HttpServletRequest request,
			@RequestParam(value="idcat", required=false) String sidcat,
			@RequestParam(value="pag", required=false) String page) {

		String PAGALIAS =Constants.CATALIAS;

		String sRet = checkCommons(request);
		if (StringUtils.isNotEmpty(sRet) && sRet.contains("/svsession/"))
			return sRet;

		try {

			headermenu.setCategory(cMenu, listMenu);
			headermenu.getParsed(model, param);

			model.addAttribute("subscribed", subscrito);

			// Buscamos la categoia a mostar
			// Chequear si hay algo del estilo /categoria/ en la url por que detras vendra la amigable
			long idcat = 0;
			Category cat = null;
			String url = request.getRequestURI();

			int currentPage=0;
			String pagination="";

			if(url.contains(PAGALIAS)) {
				String friendurl = url.substring(url.indexOf(PAGALIAS) + PAGALIAS.length());
				cat = catdao.getCatFromFriendUrl(friendurl, ows.getIdlanguaje(), catMenu);
				pagination = request.getRequestURL().toString()+"?pag=";
			}
			if (cat == null || cat.getIdCategory() <= 0 && request.getParameter("idcat")!=null) {
				idcat = Long.parseLong(request.getParameter("idcat"));
				cat = catdao.getCategory(idcat);
				pagination = request.getRequestURL().toString()+"?idcat="+idcat+"&pag=";
			}
			idcat = cat.getIdCategory();

			boolean legacy = ows.getDevice().getGama().getIdGama() == 4;

			if (page!=null){
				currentPage=Integer.parseInt(page);
			}else{
				currentPage=0;
			}

			Content dest = null;
			List <Content> cntsCat = null;

			if (cat.getIdCategoryType()==Constants.CAT_TYPE_VIDEO){

				cntsCat = cntdao.getVideoCnts(catMenu, currentPage * limitepags, limitdest, true);
				if (cntsCat!=null){
					paginacionVideos(model,catMenu,currentPage);
				}
				article2colos.paramCnt();
				article2colos.setContent(cMenu, cntsCat);
				article2colos.getParsed(model, param, "_3");

				model.addAttribute("videos",true);
			}else{

				model.addAttribute("videos",false);

				dest = cntdao.getCntDestCat(idcat, true);
				long idCntDest = 0;

				if (dest!=null){
					idCntDest=dest.getIdContent();
					cntsCat = cntdao.getCntsCategory(idcat, currentPage*limitepags, limitdest, true, idCntDest);
				}else {
					cntsCat = cntdao.getCntsCategory(idcat, currentPage * limitepags, limitdest + 1, true, idCntDest);
					dest = cntsCat.get(0);
					cntsCat = cntsCat.subList(1, cntsCat.size());
				}				
				
				banner.paramCnt();
				banner.setContent(dest);
				banner.getParsed(model, param, "_1");
				
				Content cntVideo=cntdao.getCntVidCat(idcat,true);
				List<Content>videos = new ArrayList<Content>();
				if (cntVideo!=null){
					videos.add(cntVideo);

					article2colos.paramCnt();
					article2colos.setContent(cMenu, videos);
					article2colos.getParsed(model, param, "_4");
				}

				Set<Category> cats = null;
				try{
					cats = dest.getCategories();
					if (cat!=null){
						Category destCat = cats.iterator().next();
						long idCatDest=destCat.getIdCategory();
						model.addAttribute("catDest", textdao.getCatTextByType(idCatDest,(long)Constants.TEXT_ID_TITLE).getText());
					}
				}catch(Exception e){
					log.log(Level.ERROR, "Error en categoria: " + e.getMessage());
				}

				article2colos.paramCnt();
				article2colos.setContent(cMenu, cntsCat);
				article2colos.getParsed(model, param, "_1");

				if (cntsCat!=null){
					paginacionCntsTouch(model, idcat, currentPage);
				}
			}

			model.addAttribute("prvpage", pagination+(currentPage-1));
			model.addAttribute("nxtpage", pagination+(currentPage+1));

			article2col.paramCat();
			article2col.setCategory(cMenu, listMenu);
			article2col.getParsed(model, param);

			sRet = gama + "category";
		} catch (Exception e) {
			log.log(Level.ERROR, "Error en categoria: " + e.getMessage());
		}

		return sRet;
	}
}
