 package com.owo.mwc;

 import com.owo.dao.domain.Constants;
 import com.owo.dao.domain.Texts;
 import com.owo.dao.domain.Tools;
 import com.owo.dao.domain.category.Category;
 import com.owo.dao.domain.content.Content;
 import com.owo.mwc.PadresController;
 import com.owo.mwc.parsed.*;
 import org.apache.commons.lang3.StringUtils;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.stereotype.Controller;
 import org.springframework.ui.Model;
 import org.springframework.web.bind.annotation.RequestMapping;
 import org.springframework.web.bind.annotation.RequestParam;

 import javax.servlet.http.HttpServletRequest;
 import java.util.Iterator;
 import java.util.List;
 import java.util.Set;

 @Controller
 public class ContentController extends PadresController {

     @Autowired
     HeaderMenu headermenu;
     @Autowired
     Banner banner;
     @Autowired
     Title title;
     @Autowired
     IntroNews intronews;
     @Autowired
     ContentLongparagraph longp;
     @Autowired
     Article2ColOS article2ColOS;
     @Autowired
     Article2Col article2col;
     @Autowired
     TaskList tasklist;
     @Autowired
     Videos video;

     @RequestMapping(value={"/premium/content.htm**", "/premium/content/*/**"})
     public String content(Model model,HttpServletRequest request,
             @RequestParam(value="idcnt", required=false)String sidcnt,
             @RequestParam(value="idcat", required=false)String sidcat) {

         String PAGALIAS = Constants.CNTALIAS;

         String url = Tools.getUrl(request, true);

         String sRet = checkCommons(request);

         if (StringUtils.isNotEmpty(sRet) && sRet.contains("/svsession/"))
             return sRet;

         boolean subscrito = (Boolean) ses.getAttribute(Constants.SUSBCRIBED);
 		if (!subscrito) {
 		   return "forward:" + url.replace("premium", "billingsbs");
 		}
         try {

             headermenu.setCategory(cMenu, listMenu);
             headermenu.getParsed(model, param);

             // Buscamos la categoria y el contenido a mostar
             // Chequear si hay algo del estilo /categoria/ en la url por que detras vendra la amigable
             long idcat = 0;
             long idcnt = 0;
             Category cat = null;
             Content cnt = null;
             if(url.contains(PAGALIAS)) {
                 int posfin = url.length();
                 if(url.contains("?"))
                     posfin = url.indexOf("?");
                 String friendurl = url.substring(url.indexOf(PAGALIAS) + PAGALIAS.length(), posfin);
                 cnt = cntdao.getCntFromFriendUrl(friendurl,  ows.getIdlanguaje());
                 idcnt = cnt.getIdContent();
             }

             if (cnt == null || cnt.getIdContent() <= 0) {
                 idcnt = Long.parseLong(sidcnt);
                 cnt = cntdao.getContent(idcnt);
             }

             Set <Category> catref=null;
             Iterator <Category >categories = null;
             if (cnt!=null){
                 catref = cnt.getCategories();
                 if(catref!=null){
                     categories = catref.iterator();
                 }
                 cat = catdao.getCategory(categories.next().getIdCategory());
                 if (cat.getIdCategory()==catDestacda && categories.hasNext()){
                     cat = catdao.getCategory(categories.next().getIdCategory());
                 }
             }
             if (cat != null){
                 idcat = cat.getIdCategory();
             }

             Texts catTitle =  textdao.getCatTextByType(idcat, Constants.TEXT_ID_TITLE);
             String txtTitle=null;
             if (catTitle!=null){
                 txtTitle=catTitle.getText();
             }else{
                 txtTitle=cat.getCategoryName();
             }
             model.addAttribute("catName",txtTitle);

             title.setContent(cnt);
             title.getParsed(model, param);

             if (cnt.isVideo()) {
                 video.paramCnt();
                 video.setContent(cnt);
                 video.getParsed(model, param);
                 model.addAttribute("video", true);
             }else{
                 model.addAttribute("video", false);
             }

             banner.setContent(cnt);
             banner.getParsed(model, param);

             intronews.setContent(cnt);
             intronews.getParsed(model, param);

             tasklist.setContent(cnt);
             tasklist.getParsed(model, param);

             longp.setContent(cnt);
             longp.setParam("bodynews_1");
             longp.getParsed(model, param);

             List <Content> cnts = cntdao.getContentCategoryListRnd(idcat,0,3,true);

             article2ColOS.paramCnt();
             article2ColOS.setContent(cMenu, cnts);
             article2ColOS.getParsed(model, param, "_1");


             Content prvCnt = cntdao.getPrvCnt(true, idcat, idcnt);

             if (prvCnt!=null){
                 String link="http://"+ows.getSite().getUrlBase()+"/premium";
                 model.addAttribute("prev", true);
                 Texts t = textdao.getCntTextByType(prvCnt.getIdContent(), Constants.TEXT_ID_FRIENDLY);
                 if (t==null){
                     t = textdao.getCntTextByType(prvCnt.getIdContent(), Constants.TEXT_ID_LINK);
                     link=link+"/"+t.getText();
                 }else{
                     link=link+Constants.CNTALIAS+t.getText();
                 }
                 model.addAttribute("prvcnt", link);
             }

             Content nxtCnt = cntdao.getNxtCnt(true, idcat, idcnt);
             if (nxtCnt!=null){
                 String link="http://"+ows.getSite().getUrlBase()+"/premium";
                 model.addAttribute("sig", true);
                 Texts t = textdao.getCntTextByType(nxtCnt.getIdContent(), Constants.TEXT_ID_FRIENDLY);
                 if (t==null){
                     t = textdao.getCntTextByType(nxtCnt.getIdContent(), Constants.TEXT_ID_LINK);
                     link=link+"/"+t.getText();
                 }else{
                     link=link+Constants.CNTALIAS+t.getText();
                 }
                 model.addAttribute("nxtcnt", link);
             }

         } catch (Exception e) {
             e.printStackTrace();
         }

         article2col.paramCat();
         article2col.setCategory(cMenu, listMenu);
         article2col.getParsed(model, param);

 /*		Utils utils = new Utils();
         List<Content> cnts = utils.crossContents(catRecomended, cntdao, catdao, model, log);

         article2col.paramCnt();
         article2col.setContent(catdao.getCategory(catRecomended), cnts);
         article2col.getParsed(model, param, "_1");

         cnts = utils.crossContents(catRecomended2, cntdao, catdao, model, log, "_2");

         article2col.paramCnt();
         article2col.setContent(catdao.getCategory(catRecomended2), cnts);
         article2col.getParsed(model, param, "_2");*/

         sRet = gama +"content";

         return sRet;
     }
 }
