package com.owo.mwc;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.owo.dao.domain.Constants;
import com.owo.dao.domain.Images;
import com.owo.dao.domain.Texts;
import com.owo.dao.domain.Tools;
import com.owo.dao.domain.content.Content;
import com.owo.dao.repository.TextsTypes;
import com.owo.log.Log;
import com.owo.mwc.parsed.Article1Col;
import com.owo.mwc.parsed.HeaderMenu;
import com.owo.mwc.parsed.Subtitle;
import com.owo.web.http.Params;

@Controller
public class PayCardController extends PadresController {

	@Autowired
	HeaderMenu headermenu;
	@Autowired
	Article1Col article1row;
	@Autowired
	Subtitle title;
	
	@RequestMapping(value={"/paycard.htm**", "/paycard/content/*/**", "/paycard/content.htm*",
			"/paycardinfo.htm**", "/paycardinfo/content/*/**", "/paycardinfo/content.htm*","/paycardinfo**"})
	public String paycard(Model model, HttpServletRequest request,
			@RequestParam(value="idcnt", required=false)String sidcnt,
			@RequestParam(value="idcat", required=false)String sidcat) {

		String PAGALIAS = Constants.CNTALIAS;

		String sRet = checkCommons(request);
		if (StringUtils.isNotEmpty(sRet) && sRet.contains("/svsession/"))
			return sRet;

		try {

			headermenu.setCategory(cMenu, listMenu);
			headermenu.getParsed(model, param);

			// Buscamos el contenido a mostar
			long idcnt = 0;

			Content cnt = null;
			String url = Tools.getUrl(request, true);

			if(url.contains(PAGALIAS)) {
				int posfin = url.length();
				if(url.contains("?"))
					posfin = url.indexOf("?");
				String friendurl = url.substring(url.indexOf(PAGALIAS) + PAGALIAS.length(), posfin);
				cnt = cntdao.getCntFromFriendUrl(friendurl,  ows.getIdlanguaje());
				if (cnt!=null){
					idcnt = cnt.getIdContent();
				}
			}

			if (cnt == null || cnt.getIdContent() <= 0) {	
				idcnt = Long.parseLong(request.getParameter("idcnt"));
				cnt = cntdao.getContent(idcnt);
			}

			if (url.contains("/paycard/"))
				url = url.replace("/paycard/", "/");

			String subsUrl= null;
			String userName = null;
			userName = ows.getUser().getName();
			if (userName!=null && userName.equals(Constants.UNKNOWN)){
				if (ses.getAttribute("wifi")==null){
					ses.setAttribute("wifi", true);
					return  "forward:/svsession/" + Tools.getUrl(request, true);
				}
				subsUrl = Constants.MID_REDIRECTION+Constants.REDIR_PARAMETER+ows.getSite().getUrlBase()+Constants.SVSUBS+ url; 
			}else{ 
				subsUrl = Constants.SVSUBS + url;
			}

			title.paramCnt();
			title.setContent(cnt);
			title.getParsed(model, param);

			Images img = imgdao.getCntImgByType(idcnt, Constants.IMG_ID_GIF);
			if (img==null){
				img = imgdao.getCntImgByType(idcnt, Constants.IMG_ID_BANNER);
			}
			if (img!=null){
				model.addAttribute("img", img.getPath());
			}else {
				File folder = new File(path+"/paycard/gifs");
				String[] gifs = folder.list();
				if (gifs!=null && gifs.length>0){
					int length = gifs.length-1;
					boolean gifExists=false;
					int gif=0;
					while (gifExists==false){
						gif = (int) Math.floor(Math.random()*(length));
						if (gifs[gif].contains(".gif")){
							model.addAttribute("img", Constants.PAY_GIFS_PATH+"/"+gifs[gif]);
							gifExists=true;
						}
					}
				}
			}

			sRet = gama + "paycard";

			List<Content> lst = new ArrayList<Content>();
			Content cnt2 = cnt;
			Set<Texts> texts = cnt2.getTexts();
			Set<Texts> texts2 = new HashSet<Texts>();

			for(Texts tt : texts) {
				TextsTypes type = TextsTypes.getTextsType(tt.getIdTextType());
    
				if ((type.getValue()!=15) && (type.getValue()!=20)){ 
					texts2.add(tt);
					
				}
			}
			cnt2.setTexts(texts2);
			lst.add(cnt2);

			//Control para mostrar la carta de pago para usuarios noveles o veteranos en funci�n dle cpc usado para cobrar
			String cpc = (String)ses.getAttribute("CPC");
			if(StringUtils.isNotEmpty(cpc) && Integer.parseInt(cpc)==cpcFomento)
				model.addAttribute("COMPLETA", true);
			else
				model.addAttribute("COMPLETA", false);

			model.addAttribute("cancel", "/");
			model.addAttribute("subscribe", subsUrl);
			model.addAttribute(Params.ARTICLE1COLCO, lst.iterator());

		} catch (NumberFormatException nfe) {
			Log.tracer(log, Level.ERROR, "Exception", nfe);
		}

		return sRet;
	}

	@RequestMapping(value={"/payok.htm**", "/payok/content/*/**"})
	public String payok(Model model, HttpServletRequest request,
			@RequestParam(value="idcnt", required=false)String sidcnt,
			@RequestParam(value="idcat", required=false)String sidcat) {
	
		String PAGALIAS = Constants.CNTALIAS;

		String url = Tools.getUrl(request, true);
		String sRet = checkCommons(request);
		if (StringUtils.isNotEmpty(sRet) && sRet.contains("/svsession/"))
			return sRet + url;
		
		try {
			
			headermenu.setCategory(cMenu, listMenu);
			headermenu.getParsed(model, param);
		
			// Buscamos el contenido a mostar
			long idcnt = 0;
	
			Content cnt = null;

			String cntUrl = "";
			if(url.contains(PAGALIAS)) {
				int posfin = url.length();
				if(url.contains("?"))
					posfin = url.indexOf("?");
				String friendurl = url.substring(url.indexOf(PAGALIAS) + PAGALIAS.length(), posfin);
				cnt = cntdao.getCntFromFriendUrl(friendurl,  ows.getIdlanguaje());
				cntUrl = "premium" + url.substring(url.indexOf(PAGALIAS));
	
			}
	
			if (cnt == null || cnt.getIdContent() <= 0) {
				idcnt = Long.parseLong(sidcnt);				
				cnt = cntdao.getContent(idcnt);
				cntUrl = "premium/content.htm?idcnt=" + idcnt;
			}
			idcnt = cnt.getIdContent();
		
			model.addAttribute("link", cntUrl);
			sRet = gama + "payok";
			return sRet;
	
		} catch (NumberFormatException nfe) {
			
		}

		return sRet;
	}
}
