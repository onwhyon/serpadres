package com.owo.mwc;

import com.owo.dao.domain.Constants;
import com.owo.dao.domain.Tools;
import com.owo.dao.domain.category.Category;
import com.owo.dao.domain.session.OwoSession;
import com.owo.dao.repository.ImageDao;
import com.owo.dao.repository.TextDao;
import com.owo.dao.repository.category.CategoryDao;
import com.owo.dao.repository.content.ContentDao;
import com.owo.dao.repository.session.SessionDao;
import com.owo.log.Log;
import com.owo.web.http.Params;
import org.apache.logging.log4j.core.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@PropertySource({"classpath:/META-INF/cats.properties"})
public class PadresController {
	@Value("${cat.menu}")
	protected long catMenu;
	@Value("${cat.destacada}")
	protected long catDestacda;
	@Value("${cat.promotion1}")
	protected long catPromo1;
	@Value("${cat.promotion2}")
	protected long catPromo2;
	@Value("${cat.recomended}")
	protected long catRecomended;
	@Value("${cat.poll}")
	protected long catPoll;
	@Value("${cat.notification}")
	protected long catNotif;
	@Value("${idSubscription}")
	protected int idSubscription;
	@Value("${limitepags}")
	protected int limitepags;
	@Value("${limitdest}")
	protected int limitdest;
	@Value("${paycardnumgifs}")
	protected int paycardnumgifs;
	@Value("${path.images}")
	protected String path;
	@Value("${cpc.fomento}")
	protected int cpcFomento;
	//You need this
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Autowired
	CategoryDao catdao;
	@Autowired
	ContentDao cntdao;
	@Autowired
	TextDao textdao;
	@Autowired
	ImageDao imgdao;
	@Autowired
	SessionDao sesdao;

	protected Params param;
	protected String gama;
	protected Category cMenu;
	protected List<Category> listMenu;
	protected OwoSession ows;
	protected HttpSession ses;
	protected boolean subscrito;
	protected boolean redirected;
	Logger log = null;
	protected boolean wifi;
	@SuppressWarnings("unchecked")
	protected String checkCommons(HttpServletRequest request) {

		ses = request.getSession();
		
		ses.setAttribute("idSubscription", idSubscription);
		// Pedimos el log con el subdominio para los pruebas en local
		if(request.getServerName().contains("localhost")) {
			log = Log.getLog("yomujer");
			ses.setAttribute(Constants.LOG, log);
		}
		log = Log.getLog(request);

		String sRet = null;

		ows = (OwoSession) ses.getAttribute(Constants.SES);
		
		if (ows == null) {
			sRet =  "forward:/svsession/" + Tools.getUrl(request, true);
		} 
		else{
			
			subscrito = (Boolean) ses.getAttribute(Constants.SUSBCRIBED);
			
			if (ows.getUser().getName().equals(Constants.UNKNOWN)){
				wifi = true;
			}else{
				wifi = false;
			}
			
			param = (Params) ses.getAttribute(Constants.PARAMS);
			param.setParam(Params.HEADERMENU, String.valueOf(catMenu));
			
			gama = ows.getGama();	
	
			cMenu = (Category) ses.getAttribute(Constants.CATMENU);
			if (cMenu == null || cMenu.getIdCategory() <= 0) {
				cMenu =	catdao.getCategory(catMenu);
				ses.setAttribute(Constants.CATMENU, cMenu);
			}
			listMenu = (List<Category>) ses.getAttribute(Constants.MENU);
			if (listMenu == null || listMenu.size() <= 0) {
				listMenu = catdao.getCategoryList(catMenu, 0, 0);
				ses.setAttribute(Constants.MENU, listMenu);
			}
		}
	
		return sRet;
	}
	
	protected void paginacionCntslegacy (Model model, long idcat, int currentPage){

		int longitud= cntdao.getContentCategoryList(idcat, 0, 0).size();
		int lim = (int) Math.ceil((double)longitud/(double)limitepags);

		pagButtons(model, lim, currentPage);
	}
	
	protected void pagButtons (Model model, int lim, int currentPage){
		
		if (currentPage==0)
			model.addAttribute("prev", false);
		else 
			model.addAttribute("prev", true);
		
		if (currentPage<lim-1)
			model.addAttribute("sig", true);
		else 
			model.addAttribute("sig", false);
	}

	protected void paginacionCntsTouch (Model model, long idcat, int currentPage){

		int longitud= cntdao.getCntsCategory(idcat, 0, 0, true, 0).size()-1;
		int lim = (int) Math.ceil((double)longitud/(double)limitepags);

		pagButtons(model, lim, currentPage);
	}

	protected void paginacionVideos (Model model, long idcat, int currentPage){

		int longitud= cntdao.getVideoCnts(idcat, 0, 0, true).size()-1;
		int lim = (int) Math.ceil((double)longitud/(double)limitepags);

		pagButtons(model, lim, currentPage);
	}
}
